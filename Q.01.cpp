#include <stdio.h>
struct student
{
	char name[100];
    char subject[100];
    int marks;
};
int main()
{
	int i,n;
        printf("Enter how many records u want to store = ");
        scanf("%d",&n);
        struct student stuarr[n];
        // storing information
        for(i=0; i<n; i++)
        {
            printf("\nEnter %d record, \n",i+1);
            printf("Enter the first name : ");
            scanf("%s",&stuarr[i].name);
            printf("Enter Subject : ");
            scanf("%s",&stuarr[i].subject);
            printf("Enter Marks : ");
            scanf("%i",&stuarr[i].marks);
        }
        printf("\nDisplaying Information:");
        printf("\n_______________________\n");
        for(i=0; i<n; i++) 
		{
            printf("\nStudent number: %d\n", i + 1);
            printf("Name: ");
            puts(stuarr[i].name);
            printf("Subject: ");
            puts(stuarr[i].subject);
            printf("Marks: %.1i", stuarr[i].marks);
            printf("\n");
        }
    return 0;
}
